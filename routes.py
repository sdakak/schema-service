from flask import jsonify
from flask import render_template
from flask import flash
from flask import current_app
from flask import abort

from middleware import schema_node_by_id
from middleware import schema_node
from middleware import add_schema_node
from middleware import update_schema_node
from middleware import delete_schema_node
from middleware import change_by_id
from middleware import change
from middleware import add_change
from middleware import update_change
from middleware import delete_change
from middleware import schema_nested
from middleware import schema_proposed
from middleware import schema_proposed_text
from middleware import usage
from middleware import usage_test
from middleware import initialize_database as init_db
from middleware import fill_database as fill_db
from middleware import build_message


def init_api_routes(app):
    if app:
        app.add_url_rule('/api/schema/<string:id>', 'schema_node_by_id', schema_node_by_id, methods=['GET'])
        app.add_url_rule('/api/schema', 'schema', schema_node, methods=['GET'])
        app.add_url_rule('/api/schema', 'add_schema_node', add_schema_node, methods=['POST'])
        app.add_url_rule('/api/schema/<string:id>', 'update_schema_node', update_schema_node, methods=['PUT'])
        app.add_url_rule('/api/schema/delete/<string:id>', 'delete_schema_node', delete_schema_node, methods=['DELETE'])

        app.add_url_rule('/api/schema/proposed/<string:id>', 'schema_proposed', schema_proposed, methods=['GET'])
        app.add_url_rule('/api/schema/proposed/text/<string:id>', 'schema_proposed_text', schema_proposed_text, methods=['GET'])
        app.add_url_rule('/api/schema/nested', 'schema_nested', schema_nested, methods=['GET'])

        app.add_url_rule('/api/usage', 'usage', usage, methods=['POST'])
        app.add_url_rule('/api/usage/test', 'usage_test', usage_test, methods=['POST'])

        app.add_url_rule('/api/change/<string:id>', 'change_by_id', change_by_id, methods=['GET'])
        app.add_url_rule('/api/change', 'change', change, methods=['GET'])
        app.add_url_rule('/api/change', 'add_change', add_change, methods=['POST'])
        app.add_url_rule('/api/change/<string:id>', 'update_change', update_change, methods=['PUT'])
        app.add_url_rule('/api/change/delete/<string:id>', 'delete_change', delete_change, methods=['DELETE'])

        app.add_url_rule('/api/initdb', 'initdb', initialize_database)
        app.add_url_rule('/api/filldb', 'filldb', fill_database)
        app.add_url_rule('/api', 'list_routes', list_routes, methods=['GET'], defaults={'app': app})


def page_about():
    if current_app:
        pass
        # flash('The application was loaded', 'info')
        # flash('The secret key is {0}'.format(current_app.config['SECRET_KEY']), 'info')

    return render_template('about.html', selected_menu_item="about")


def page_schema_node():
    current_schema_nodes = schema_node(serialize=False)
    nested_schema = schema_nested(serialize=False)
    return render_template('schema.html', selected_menu_item="schema", schema_nodes=current_schema_nodes, nested_schema=nested_schema)

def page_change():
    current_change = change(serialize=False)
    return render_template('change.html', selected_menu_item="change", changes=current_change)

def page_add_schema_node():
    return render_template('add_schema_node.html')

def page_add_change():
    return render_template('add_change.html')

def page_usage():
    return render_template('usage.html')

def page_index():
    return render_template('index.html', selected_menu_item="index")


def crash_server():
    abort(500)


def initialize_database():
    message_key = "Initialize Database"
    try:
        init_db()
    except ValueError as err:
        return jsonify(build_message(message_key, err.message))

    return jsonify(build_message(message_key, "OK"))


def fill_database():
    message_key = "Fill Database"
    try:
        fill_db()
    except ValueError as err:
        return jsonify(build_message(message_key, err.message))

    return jsonify(build_message(message_key, "OK"))


def init_website_routes(app):
    if app:
        app.add_url_rule('/crash', 'crash_server', crash_server, methods=['GET'])
        app.add_url_rule('/about', 'page_about', page_about, methods=['GET'])
        app.add_url_rule('/schema', 'page_schema_node', page_schema_node, methods=['GET'])
        app.add_url_rule('/schema/add', 'page_add_schema_node', page_add_schema_node, methods=['GET'])
        app.add_url_rule('/change', 'page_change', page_change, methods=['GET'])
        app.add_url_rule('/change/add', 'page_add_change', page_add_change, methods=['GET'])
        app.add_url_rule('/usage', 'page_usage', page_usage, methods=['GET'])
        app.add_url_rule('/', 'page_index', page_index, methods=['GET'])


def handle_error_404(error):
    flash('Server says: {0}'.format(error), 'error')
    return render_template('404.html', selected_menu_item=None)


def handle_error_500(error):
    flash('Server says: {0}'.format(error), 'error')
    return render_template('500.html', selected_menu_item=None)


def init_error_handlers(app):
    if app:
        app.error_handler_spec[None][404] = handle_error_404
        app.error_handler_spec[None][500] = handle_error_500


def list_routes(app):
    result = []
    for rt in app.url_map.iter_rules():
        result.append({
            'methods': list(rt.methods),
            'route': str(rt)
        })
    return jsonify({'routes': result, 'total': len(result)})
