class node():
    def __init__(self, value, usage=0):
        self.value = value
        self.usage = usage
        self.children = []

    def add_child(self, obj):
        self.children.append(obj)

    # in order traversal that returns dict
    def as_dict(self, show_usage=False):
        if (show_usage):
            res = {'name': self.value.name, 'usage': self.usage}
        else:
            res = {'name': self.value.name}
        if self.children:
            res['children'] = [c.as_dict(show_usage) for c in self.children]
        return res

    # in order traversal that returns indented text
    def as_nested(self, count=0, show_usage=False):
        output = ' ' * count + self.value.name
        if show_usage:
            output += ' ' + str(self.usage)
        output += '\n'
        count += count + 1 if self.children else count - 1
        for c in self.children:
            output += c.as_nested(count, show_usage)
        return output
