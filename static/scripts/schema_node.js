function saveSchemaNode() {
    var name = $("#inputName").val();
    var child_of = $("#inputChildOf").val();

    var success_alert = $("#success_alert");

    $.post("/api/schema", {
                name: name,
                child_of: child_of}, function(response){
                    $(success_alert).find("#success_message").text("Schema Node successfully created with ID:" + response.id);
                    $(success_alert).removeClass("hidden");
    });
}
