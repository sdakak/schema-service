function saveChange() {
    var status = $("#inputStatus").val();
    var description = $("#inputDesc").val();
    var user = $("#inputUser").val();
    var sn_name = $("#inputSNName").val();
    var sn_child_of = $("#inputSNChildOf").val();

    var success_alert = $("#success_alert");

    $.post("/api/change", {
                status : status,
                description: description,
                user: user,
                sn_name: sn_name,
                sn_child_of: sn_child_of}, function(response){
                    $(success_alert).find("#success_message").text("Schema Change Proposal successfully created with ID:" + response.id);
                    $(success_alert).removeClass("hidden");
    });
}
function showProposedSchema(id) {
    var proposed_schema = 'abc'
    $.get("/api/schema/proposed/text/"+id, function(response) {
        var x = document.getElementById('proposedSchemaDiv');
        x.innerHTML = '<h4> Effect of ChangeID ' + id + ' on Schema</h4>' +
            '<pre>' + response + '<pre>'
    });
}
