function saveChange() {
    var status = $("#inputStatus").val();
    var description = $("#inputDesc").val();
    var user = $("#inputUser").val();
    var sn_name = $("#inputSNName").val();
    var sn_child_of = $("#inputSNChildOf").val();

    var success_alert = $("#success_alert");

    $.post("/api/change", {
                status : status,
                description: description,
                user: user,
                sn_name: sn_name,
                sn_child_of: sn_child_of}, function(response){
                    $(success_alert).find("#success_message").text("Schema Change Proposal successfully created with ID:" + response.id);
                    $(success_alert).removeClass("hidden");
    });
}
function getUsage() {
    $.post("/api/usage/test", {}, function(response) {
            var x = document.getElementById('leafDiv');
            x.innerHTML = '<h4>Request sent to service with following leaf node aggregated usage </h4>' +
                '<pre> ' +
                'POST /api/usage\n        ' +
                '\n' +
                'incoming_sms: 50\n' +
                'outgoing_sms_us: 40\n' +
                'outgoing_sms_uk: 10\n' +
                'incoming_calls_us: 400\n' +
                'incoming_calls_voicemail: 100\n' +
                'outgoing_calls_us_regular: 300\n' +
                'outgoing_calls_us_tollfree: 100\n' +
                'outgoing_calls_uk: 100 </pre>'
            var y = document.getElementById('rolledUpUsageDiv');
            y.innerHTML = '<h4>Response received from service showing rolled up usage for the account </h4>' +
                '<pre>' + response + '</pre>'
    });
}
