import operator

from flask import jsonify
from flask import abort
from flask import make_response
from flask import request
from flask import url_for
from Models import SchemaNode

from node import node

from data_provider_service import DataProviderService
import json

# db_engine = 'mysql+mysqldb://root:@localhost/flask_api'
db_engine = 'mysql://c1ubi9gbya5l2ju4:bxz9d8tkeep4tjhh@p1us8ottbqwio8hv.cbetxkdyhwsb.us-east-1.rds.amazonaws.com:3306/n3ysjdcp27urxpdt'

DATA_PROVIDER = DataProviderService(db_engine)

def usage_test():
    test_usage = ["3 50", "5 40", "6 10", "9 400", "10 100", "13 300", "14 100", "16 100"]
    form_usage_tuples = [tuple(int(y) for y in usg_pair.split()) for usg_pair in test_usage]
    return usage_internal(form_usage_tuples, False)

def usage(serialize = True):
    form_usage = request.form.getlist("usg")
    form_usage_tuples = [tuple(int(y) for y in usg_pair.split()) for usg_pair in form_usage]
    return usage_internal(form_usage_tuples, serialize)

def usage_internal(leaf_usage_tuples, serialize = True):
    # Get the schema
    schema_nodes_list = schema_node(serialize=False)
    # Create a dict with id : TreeNode(SchemaNode)
    schema_nodes_dict = dict((e.id, node(e)) for e in schema_nodes_list)
    # Create links to all parents
    create_links(schema_nodes_list, schema_nodes_dict, children=False)

    # Bubble up usage to all parents
    for key, usg in leaf_usage_tuples:
        nd = schema_nodes_dict[key]
        nd.usage += usg
        for c in nd.children:
            schema_nodes_dict[c.value.id].usage += usg

    # Create links to all children
    create_links(schema_nodes_list, schema_nodes_dict, children=True)
    root = schema_nodes_dict[1]
    # Links to children allow us to do in order traversal
    if serialize:
        return json.dumps(root.as_dict(show_usage=True), indent=1)
    else:
        return root.as_nested(0, True)

def schema_nested(serialize = True):
    schema_nodes_list = schema_node(serialize=False)
    return schema_nested_internal(schema_nodes_list, serialize)

def create_links(schema_nodes_list, schema_nodes_dict, children=True):
    # todo we could store node.children and node.parents both. Memory vs time trade-off here

    # clear all links
    for k, v in schema_nodes_dict.iteritems():
        v.children = []

    # create links to children
    if children:
        for e in schema_nodes_list:
            if e.child_of != 0:
                schema_nodes_dict[e.child_of].add_child(schema_nodes_dict[e.id])
    # create links to parents
    else:
        for e in schema_nodes_list:
            if e.id != 1:
                ancestor = schema_nodes_dict[e.child_of]
                while ancestor.value.id != 1:
                    schema_nodes_dict[e.id].add_child(schema_nodes_dict[ancestor.value.id])
                    ancestor = schema_nodes_dict[ancestor.value.child_of]
                schema_nodes_dict[e.id].add_child(schema_nodes_dict[1])

    return schema_nodes_dict

def schema_nested_internal(schema_nodes_list, serialize = True):
    # Create a dict with id : TreeNode(SchemaNode)
    schema_nodes_dict = dict((e.id, node(e)) for e in schema_nodes_list)
    # Create links to Children
    root = create_links(schema_nodes_list, schema_nodes_dict)[1]
    # Links to children allow us to do in order traversal
    result = root.as_dict()
    if serialize:
        return json.dumps(result, indent=1)
    else:
        return root.as_nested()

def create_schema_node_from_change_id(id, schema_nodes_list, capitalize_name=False):
    change = DATA_PROVIDER.get_change(id, serialize=False)[0]

    # Create Schema Node from this ChangeID with SchemaNodeID higher than the highest Schema Node in DB
    index, value = max(enumerate(schema_nodes_list), key=operator.itemgetter(1))
    sn = SchemaNode()
    sn.id = value.id + 1
    sn.name = change.sn_name
    if capitalize_name:
        sn.name = sn.name.upper()
    sn.child_of = change.sn_child_of
    return sn;

def schema_proposed_internal(id, capitalize_name=False):
    # Retrieve schema
    schema_nodes_list = schema_node(serialize=False)

    # create schema node from given change
    sn = create_schema_node_from_change_id(id, schema_nodes_list, capitalize_name=capitalize_name)

    # Add this proposed schema node to the internal list representing schema
    schema_nodes_list.append(sn)
    return schema_nodes_list

def schema_proposed(id):
    # Construct nested json representation of proposed schema
    return schema_nested_internal(schema_proposed_internal(id))

def schema_proposed_text(id):
    # Construct nested textual representation of proposed schema
    return schema_nested_internal(schema_proposed_internal(id, capitalize_name=True), serialize=False)

def schema_node(serialize = True):
    schema_nodes = DATA_PROVIDER.get_schema_node(serialize=serialize)
    if serialize:
        return jsonify({"schema_nodes": schema_nodes, "total": len(schema_nodes)})
    else:
        return schema_nodes


def schema_node_by_id(id):
    current_schema_node = DATA_PROVIDER.get_schema_node(id, serialize=True)
    if schema_node:
        return jsonify({"schema_node": current_schema_node})
    else:
        #
        # In case we did not find the schema_node by id
        # we send HTTP 404 - Not Found error to the client
        #
        abort(404)

def change(serialize = True):
    changes = DATA_PROVIDER.get_change(serialize=serialize)
    if serialize:
        return jsonify({"changes": changes, "total": len(changes)})
    else:
        return changes


def change_by_id(id):
    change = DATA_PROVIDER.get_change(id, serialize=True)
    if change:
        return jsonify({"change": change})
    else:
        #
        # In case we did not find the schema_node by id
        # we send HTTP 404 - Not Found error to the client
        #
        abort(404)

def initialize_database():
    DATA_PROVIDER.init_database()


def fill_database():
    DATA_PROVIDER.fill_database()

def delete_schema_node(id):
    if DATA_PROVIDER.delete_schema_node(id):
        return make_response('', 200)
    else:
        return abort(404)

def delete_change(id):
    if DATA_PROVIDER.delete_change(id):
        return make_response('', 200)
    else:
        return abort(404)

def update_schema_node(id):
    new_schema_node = {
        "name":request.form["name"],
        "child_of":request.form["child_of"]
    }
    updated_schema_node = DATA_PROVIDER.update_schema_node(id, new_schema_node)
    if not updated_schema_node:
        abort(404)
    else:
        return jsonify({"schema_node": updated_schema_node})

def update_change(id):
    new_change = {
        "status":request.form["status"],
        "description":request.form["description"],
        "user":request.form["user"],
        "sn_name":request.form["sn_name"],
        "sn_child_of":request.form["sn_child_of"]
    }
    updated_change = DATA_PROVIDER.update_change(id, new_change)
    if not updated_change:
        abort(404)
    else:
        return jsonify({"change": updated_change})

def add_schema_node():
    name = request.form["name"]
    child_of = request.form["child_of"]

    new_schema_node_id = DATA_PROVIDER.add_schema_node(name=name,
                                                   child_of=child_of)

    return jsonify({
        "id": new_schema_node_id,
        "url": url_for("schema_node_by_id", id=new_schema_node_id)
    })

def add_change():
    status = request.form["status"]
    description = request.form["description"]
    user = request.form["user"]
    sn_name = request.form["sn_name"]
    sn_child_of = request.form["sn_child_of"]

    new_change_id = DATA_PROVIDER.add_change(status=status,
                                             description=description,
                                             user=user,
                                             sn_name=sn_name,
                                             sn_child_of=sn_child_of)

    return jsonify({
        "id": new_change_id,
        "url": url_for("change_by_id", id=new_change_id)
    })


def build_message(key, message):
    return {key:message}
