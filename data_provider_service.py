from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from Models import SchemaNode
from Models import Change
from Models import init_database



class DataProviderService:
    def __init__(self, engine):
        """
        :param engine: The engine route and login details
        :return: a new instance of DAL class
        :type engine: string
        """
        if not engine:
            raise ValueError('The values specified in engine parameter has to be supported by SQLAlchemy')
        self.engine = engine
        db_engine = create_engine(engine)
        db_session = sessionmaker(bind=db_engine)
        self.session = db_session()

    def init_database(self):
        pass
        """
        Initializes the database tables and relationships
        :return: None
        """
        init_database(self.engine)

    def add_schema_node(self, name, child_of):
        """
        Creates and saves a new schema node to the database.

        :param name: First Name of the new schema node
        :param child_of: Parent of the new schema node
        :return: The id of the new Node
        """

        new_schema_node = SchemaNode(name=name,
                                     child_of=child_of)

        self.session.add(new_schema_node)
        self.session.commit()

        return new_schema_node.id

    def get_schema_node(self, id=None, serialize=False):
        """
        If the id parameter is  defined then it looks up the schema_node with the given id,
        otherwise it loads all schema_nodes

        :param id: The id of the schema which needs to be loaded (default value is None)
        :return: The schema_node or all schema nodes.
        """

        all_schema_nodes = []

        if id is None:
            all_schema_nodes = self.session.query(SchemaNode).order_by(SchemaNode.id).all()
        else:
            all_schema_nodes = self.session.query(SchemaNode).filter(SchemaNode.id == id).all()

        if serialize:
            return [sn.serialize() for sn in all_schema_nodes]
        else:
            return all_schema_nodes

    def update_schema_node(self, id, new_schema_node):
        updated_schema_node = None
        schema_node = self.get_schema_node(id)[0]

        if schema_node:
            schema_node.name = new_schema_node["name"]
            schema_node.child_of = new_schema_node["child_of"]
            self.session.add(schema_node)
            self.session.commit()
            updated_schema_node = self.get_schema_node(id)[0]

        return updated_schema_node.serialize()

    def delete_schema_node(self, id):
        if id:
            candidate_schema_node = self.get_schema_node(id)[0]
            self.session.delete(candidate_schema_node)
            self.session.commit()
            if candidate_schema_node:
                return True
        return False

    def add_change(self, status, description, user, sn_name, sn_child_of):
        """
        Creates and saves a new schema change request to the database.
        """

        new_change = Change(status=status,
                            description=description,
                            user=user,
                            sn_name=sn_name,
                            sn_child_of=sn_child_of)

        self.session.add(new_change)
        self.session.commit()

        return new_change.id

    def get_change(self, id=None, serialize=False):
        """
        If the id parameter is  defined then it looks up the change with the given id,
        otherwise it loads all the proposed changes
        """

        all_changes = []

        if id is None:
            all_changes = self.session.query(Change).order_by(Change.id).all()
        else:
            all_changes = self.session.query(Change).filter(Change.id == id).all()

        if serialize:
            return [chg.serialize() for chg in all_changes]
        else:
            return all_changes

    def update_change(self, id, new_change):
        updated_change = None
        change = self.get_change(id)[0]

        if change:
            change.status = new_change["status"]
            change.description = new_change["description"]
            change.user = new_change["user"]
            change.sn_name = new_change["sn_name"]
            change.sn_child_of = new_change["sn_child_of"]
            self.session.add(change)
            self.session.commit()
            updated_change = self.get_change(id)[0]

        return updated_change.serialize()

    def delete_change(self, id):
        if id:
            candidate_change = self.get_change(id)[0]
            self.session.delete(candidate_change)
            self.session.commit()
            if candidate_change:
                return True
        return False

    def fill_database(self):
        pass
        #
        # Schema Nodes
        #
        sn1 = SchemaNode(name="root",
                          child_of=0)
        sn2 = SchemaNode(name="sms",
                         child_of=1)
        sn3 = SchemaNode(name="incoming_sms",
                         child_of=2)
        sn4 = SchemaNode(name="outgoing_sms",
                         child_of=2)
        sn5 = SchemaNode(name="outgoing_sms_us",
                         child_of=4)
        sn6 = SchemaNode(name="outgoing_sms_uk",
                         child_of=4)
        sn7 = SchemaNode(name="calls",
                         child_of=1)
        sn8 = SchemaNode(name="incoming_calls",
                         child_of=7)
        sn9 = SchemaNode(name="incoming_calls_us",
                         child_of=8)
        sn10 = SchemaNode(name="incoming_calls_voicemail",
                         child_of=8)
        sn11 = SchemaNode(name="outgoing_calls",
                         child_of=7)
        sn12 = SchemaNode(name="outgoing_calls_us",
                         child_of=11)
        sn13 = SchemaNode(name="outgoing_calls_us_regular",
                         child_of=12)
        sn14 = SchemaNode(name="outgoing_calls_us_tollfree",
                         child_of=12)
        sn15 = SchemaNode(name="outgoing_calls_us_premium",
                         child_of=12)
        sn16 = SchemaNode(name="outgoing_calls_uk",
                         child_of=11)
        sn17 = SchemaNode(name="outgoing_calls_fr",
                         child_of=11)

        self.session.add(sn1)
        self.session.add(sn2)
        self.session.add(sn3)
        self.session.add(sn4)
        self.session.add(sn5)
        self.session.add(sn6)
        self.session.add(sn7)
        self.session.add(sn8)
        self.session.add(sn9)
        self.session.add(sn10)
        self.session.add(sn11)
        self.session.add(sn12)
        self.session.add(sn13)
        self.session.add(sn14)
        self.session.add(sn15)
        self.session.add(sn16)
        self.session.add(sn17)
        self.session.commit()


        #
        # Changes Proposed
        #
        cn1 = Change(status="Pending Approval",
                     description="new fax product",
                     user="jdoe",
                     sn_name="fax",
                     sn_child_of=1,
                     )

        cn2 = Change(status="Pending Approval",
                     description="outgoing calls to hawaii",
                     user="jdoe",
                     sn_name="outgoing_calls_us_hawaii",
                     sn_child_of=12,
                     )

        cn3 = Change(status="Pending Approval",
                     description="incoming collect calls to us",
                     user="jdoe",
                     sn_name="incoming_calls_us_collect",
                     sn_child_of=8,
                     )

        self.session.add(cn1)
        self.session.add(cn2)
        self.session.add(cn3)
        self.session.commit()

