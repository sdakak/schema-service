from sqlalchemy import Column, String, Integer
from Model import Model


class SchemaNode(Model):
    __tablename__ = 'schema_node'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    name = Column(String(200), nullable=False)
    child_of = Column(Integer, nullable=False)

    #
    # METHODS
    #

    def serialize(self):
        return {
            "id": self.id,
            "name": self.name,
            "child_of": self.child_of
        }

    def __eq__(self, other):
        return self.id == other.id

    def __gt__(self, other):
        return self.id > other.id
