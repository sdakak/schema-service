from sqlalchemy import Column, String, Integer
from Model import Model

class Change(Model):
    __tablename__ = 'change'
    id = Column(Integer, primary_key=True, nullable=False, autoincrement=True)
    status = Column(String(200), nullable=False)
    description = Column(String(500), nullable=False)
    user = Column(String(200), nullable=False)
    sn_name = Column(String(200), nullable=False)
    sn_child_of = Column(Integer, nullable=False)

    #
    # METHODS
    #

    def serialize(self):
        return {
            "id": self.id,
            "status": self.status,
            "description": self.description,
            "user": self.user,
            "sn_name": self.sn_name,
            "sn_child_of": self.sn_child_of,
        }
